# Nucleotides Counter
The goal of this project is to show you how to build an application that will run as a backend service in a Phenex (new CGE framework). The application counts nucleotides in a [FASTA](http://www.cbs.dtu.dk/services/NetGene2/fasta.php) file.

## Requirements
* [docker](https://docker.com/) installed.


## Installation
```bash
$ git clone https://github.com/ldynia/nt-counter
$ cd nt-counter/
$ docker build -t genomicepidemiology/ntc:1.0 -f docker/Dockerfile .
```
## API

```bash
$ ntc --help
usage: ntc [-h] [-i INPUT_FILE [INPUT_FILE ...]] [-o OUTPUT_DIR]

Program counts nucleotides found in FASTA file.

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT_FILE [INPUT_FILE ...], --input-file INPUT_FILE [INPUT_FILE ...] Path to FASTA file(s). If more than one file is provided use space as separator.
  -o OUTPUT_DIR, --output-dir OUTPUT_DIR Path to output directory.

```

## Application
Using `docekr`.

```bash
$ docker run --rm genomicepidemiology/ntc:1.0 python test/test.py
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK

$ docker run --rm genomicepidemiology/ntc:1.0 ntc -i test/data/dna_one.fsa test/data/dna_two.fsa | python -m json.tool
{
    "dna_one.fsa": {
        "G": 469,
        "A": 333,
        "T": 303,
        "C": 454
    },
    "dna_two.fsa": {
        "G": 135,
        "A": 65,
        "T": 64,
        "C": 96
    }
}
```

Using `docekr-compose`.

```bash
$ docker-compose build
$ docker-compose up -d
$ docker exec ntcounter python /app/test/test.py
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK

$ docker exec ntcounter ntc -i test/data/dna_one.fsa test/data/dna_two.fsa | python -m json.tool
{
    "dna_one.fsa": {
        "G": 469,
        "A": 333,
        "T": 303,
        "C": 454
    },
    "dna_two.fsa": {
        "G": 135,
        "A": 65,
        "T": 64,
        "C": 96
    }
}
```
This is test

