class Statistic():

    def __init__(self, file):
        self.file = open(file)
        self.filename = file.split('/')[-1]

    def __del__(self):
        self.file.close()

    def count_nucleotides(self):
        results = {self.filename: {}}
        for line in self.file:
            if not line.strip().startswith('>'):
                for letter in line.strip('\n').upper():
                    if letter not in results[self.filename]:
                        results[self.filename][letter] = 1
                    else:
                        results[self.filename][letter] += 1
        return results
